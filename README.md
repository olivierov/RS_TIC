# RS_TIC
RS_TIC is an electronic design which is able to demodulate the Télé-Information Client signal (TIC) from the french power meter Linky to a standard serial signal.
The 26x48mm PCB can be equipped to provide TTL UART signal either to provide a standard RS-232 signal or to provide a serial over USB device.
The choice of the output signal type is made when you solder the component thanks to the multi footprint PCB.

USB type (top)
![RS_TIC top](RS_TIC_USB_top.png)

USB type (bottom)
![RS_TIC bottom](RS_TIC_USB_bottom.png)
